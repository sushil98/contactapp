package com.pacdevelopers.contactapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.login_email)
    EditText email;
    @BindView(R.id.login_password)
    EditText password;
    @BindView(R.id.login_login_button)
    Button login;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("please wait...");
        progressDialog.setCancelable(false);

        login.setOnClickListener(v -> {
            if (validate()) {
                new LoginBackgroundTask()
                        .execute(email.getText().toString().trim(),
                                password.getText().toString().trim());
            }
        });


    }

    private void showProgressDialog(boolean aBoolean) {

        if (!aBoolean) {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }

        if (aBoolean) {

            if (progressDialog != null && !progressDialog.isShowing())
                progressDialog.show();
        }
    }

    private boolean validate() {
        boolean valid = true;
        String strEmail = email.getText().toString();
        String strPassword = password.getText().toString();


        if (strEmail.isEmpty()) {
            email.requestFocus();
            email.setError("Enter Email Id");
            valid = false;
        } else if (strPassword.isEmpty()) {
            password.requestFocus();
            password.setError("Enter Password");
            valid = false;
        } else {
            email.setError(null);
            password.setError(null);
        }


        return valid;
    }

    public class LoginBackgroundTask extends AsyncTask<String, Void, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(true);
        }

        @Override
        protected String doInBackground(String... params) {


            String userName = params[0];
            String pass = params[1];
            String loginAPI = "http://campyen.com/api/login/" + userName + "/" + pass;
            JSONParser jp = new JSONParser();

            return jp.makeServiceCall(loginAPI);

        }

        @Override
        protected void onPostExecute(String result) {
            showProgressDialog(false);

            if (result.equalsIgnoreCase("Invalid User Id or password")) {
                Toasty.error(LoginActivity.this, result, Toast.LENGTH_LONG).show();

            } else {
                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                i.putExtra("email", result);
                startActivity(i);
                Toasty.success(LoginActivity.this, "Login Success", Toast.LENGTH_SHORT).show();
                finish();
            }


        }
    }

}
